# Copyright (C) 2023 Cooperativa de Trabajo Sutty Ltda.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

require "kemal"
require "file_utils"
require "option_parser"
require "tor_client.cr/tor/client"
require "./tor/hidden_service"

Log.setup_from_env

address = "127.0.0.1"
port : UInt16 = 9051
service_address = "127.0.0.1"
service_port : UInt16 = 80

def parse_port(port : String)
  port.to_u16.tap do |parsed_port|
    raise ArgumentError.new("") if parsed_port.zero?
  end
rescue ArgumentError
  Log.fatal &.emit("Port out of range", port: port)
  exit 1
end

OptionParser.parse do |p|
  p.banner = "Sutty Tor Hidden Service\n\nUsage: sutty_tor_hidden_service [-h] [-S 127.0.0.1] [-P 80] [-a 127.0.0.1] [-c 9051]"

  p.on("-h", "--help", "Help") do
    puts p
    exit
  end

  p.on("-a ADDRESS", "--address=ADDRESS", "Tor Control IP address") do |a|
    address = a
  end

  p.on("-c PORT", "--control-port=PORT", "Tor Control port") do |p|
    port = parse_port p
  end

  p.on("-P PORT", "--service-port=PORT", "Service port") do |p|
    service_port = parse_port p
  end

  p.on("-S SERVICE", "--service-address=SERVICE", "Service address") do |a|
    service_address = a
  end
end

Log.info &.emit("Tor Control Client", address: address, port: port.to_s)

# Keep a session open
client = Tor::Client.new address, port
client.authenticate!

# Bind to all interfaces
Kemal.config.host_binding = "::"
# Serve nothing else
serve_static false

# Generate a hidden service if it doesn't exist yet.
get "/:name" do |env|
  name = env.params.url["name"]
  hs = Tor::HiddenService.new(client, name, service_address, service_port)

  unless hs.exists?
    Log.info &.emit("Generating onion address", name: name)

    hs.create!
    hs.reload!
  end

  hs.hostname
end

Kemal.run
