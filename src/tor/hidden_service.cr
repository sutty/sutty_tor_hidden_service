# Copyright (C) 2023 Cooperativa de Trabajo Sutty Ltda.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

module Tor
  # Creates a hidden service configuration and keypair
  class HiddenService
    getter client : Client
    getter server : String
    getter port : UInt16
    getter name : String
    getter configuration_file : String
    getter service_directory : String
    getter hostname_file : String

    NAME_REGEXP = /\A[a-z0-9\-\.]+\z/

    def initialize(@client, @name, @server, @port)
      unless name =~ NAME_REGEXP
        raise ArgumentError.new("#{name} is not a valid hostname")
      end

      @configuration_file = "/var/lib/tor/hidden_services/conf.d/#{name}.conf"
      @service_directory = "/var/lib/tor/hidden_services/#{name}"
      @hostname_file = "#{service_directory}/hostname"
    end

    def exists?
      File.directory?(service_directory) && File.exists?(configuration_file)
    end

    # Wait for the hostname to exist.  Run after create and reload.
    #
    # TODO: Generate our own onion addresses
    def hostname : String
      hostname = ""
      timeout = 0

      loop do
        sleep 1
        timeout += 1

        if File.exists? hostname_file
          hostname = File.read(hostname_file).strip
          break
        end

        break if timeout >= 5
      end

      hostname
    end

    def create!
      FileUtils.mkdir_p(service_directory, 0o700)
      FileUtils.mkdir_p(File.dirname(configuration_file), 0o700)

      File.open(configuration_file, "w", 0o600) do |f|
        f.puts "# managed by sutty_tor_hidden_service"
        f.puts "HiddenServiceDir #{service_directory}"
        f.puts "HiddenServicePort 80 #{server}:#{port}"
        f.puts "HiddenServiceEnableIntroDoSDefense 1"
      end

      nil
    end

    def reload!
      client.command("SIGNAL RELOAD")
      nil
    end
  end
end
