# Sutty Tor Hidden Service

Generates hidden services on demand.

## Usage

Configure Tor:

```
```

Run as `tor` user.

```bash
sutty_tor_hidden_service --help
```

## Contributing

1. Fork it (<https://0xacab.org/sutty/sutty_tor_hidden_service/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [f](https://0xacab.org/fauno) - creator and maintainer
